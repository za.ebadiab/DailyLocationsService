package basic.android.dailylocationsservice;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;
import basic.android.dailylocationsservice.model.LatLongModel;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {


    private GoogleMap mMap;
    private double lat, lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng sydney = new LatLng(lat, lon);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        addLines();
    }

    public void addLines() {

        ArrayList<LatLng> latLngArrayList = changeModelListToLatLongList(loadLocationFromDB());


        if (latLngArrayList.size() != 0) {
            mMap.addPolyline((new PolylineOptions())
                    .addAll(latLngArrayList)
                    .width(10)
                    .color(Color.BLUE)
                    .geodesic(true));


            mMap.addMarker(new MarkerOptions().position(latLngArrayList.get(0)).title("First Location"));

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLngArrayList.get(0)));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(16));

            mMap.getUiSettings().setZoomControlsEnabled(true);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    ArrayList<LatLongModel> loadLocationFromDB() {

        return (ArrayList<LatLongModel>) LatLongModel.listAll(LatLongModel.class);

    }


    ArrayList<LatLng> changeModelListToLatLongList(List<LatLongModel> latLongModelList) {
        ArrayList<LatLng> latLngList = new ArrayList<>();


        for (LatLongModel model : latLongModelList) {
            latLngList.add(new LatLng(model.getLat(), model.getLng()));
        }

        return latLngList;
    }

}
