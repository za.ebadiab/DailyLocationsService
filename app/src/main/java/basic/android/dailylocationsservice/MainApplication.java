package basic.android.dailylocationsservice;

import com.orm.SugarApp;
import com.orm.SugarContext;

public class MainApplication extends SugarApp {
    @Override
    public void onCreate() {
        super.onCreate();

        SugarContext.init(this);

    }
}
