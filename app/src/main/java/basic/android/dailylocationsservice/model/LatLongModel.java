package basic.android.dailylocationsservice.model;


import com.orm.SugarRecord;


public class LatLongModel extends SugarRecord {

    private double lat;
    private double lng;

    public LatLongModel() {
    }

    public LatLongModel(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }


    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
