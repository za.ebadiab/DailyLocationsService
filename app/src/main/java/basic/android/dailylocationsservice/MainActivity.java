package basic.android.dailylocationsservice;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;

import basic.android.dailylocationsservice.model.LatLongModel;


public class MainActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();
        checkPermission();

    }


    void bind() {
        findViewById(R.id.btnStartLocationListService).setOnClickListener(v -> {

            startService(new Intent(mContext, LatLngListDBService.class));
        });

        findViewById(R.id.btnStopLocationListService).setOnClickListener(v -> {

            stopService(new Intent(mContext, LatLngListDBService.class));

        });

        findViewById(R.id.btnShowLocationOnMap).setOnClickListener(v -> {

            startActivity(new Intent(mContext, MapsActivity.class));
        });

        findViewById(R.id.btnDeleteDB).setOnClickListener(v -> {

            deleteTableValues();
        });
    }


    void checkPermission() {
        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 300);

        }

    }

    void deleteTableValues() {
        LatLongModel.deleteAll(LatLongModel.class);
    }


}
